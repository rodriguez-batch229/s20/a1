// Part 1

while (true) {
    let inputNum = prompt("Enter a number");
    input = Number(inputNum);

    if (inputNum === null) {
        break;
    } else if (inputNum === "") {
        alert("You did not enter any number. Please try again.");
    } else if (isNaN(input)) {
        alert("You did not enter any number. Please try again.");
    } else if (input < 50) {
        alert("You did not enter more than 50. Please try again.");
    } else {
        console.log(`The number you provided is ${inputNum}`);

        function loopNum(input) {
            for (let count = input; count >= 50; count--) {
                if (count % 10 === 0 && count > 50) {
                    console.log(
                        `The number is divisible by 10. Skipping the number`
                    );
                    continue;
                }
                if (count === 50) {
                    console.log(
                        `The current number is at ${count}. Terminating the loop`
                    );
                    break;
                }
                if (count % 5 === 0) {
                    console.log(count);
                    continue;
                }
            }
        }
        loopNum(inputNum);
        // Part 2

        let contentItems = "supercalifragilisticexpialidocious",
            storedConsonants = [];

        for (i = 0; i < contentItems.length; i++) {
            if (
                contentItems[i].toLowerCase() != "a" &&
                contentItems[i].toLowerCase() != "e" &&
                contentItems[i].toLowerCase() != "i" &&
                contentItems[i].toLowerCase() != "o" &&
                contentItems[i].toLowerCase() != "u"
            ) {
                storedConsonants.push(contentItems[i]);
                continue;
            }
        }

        console.log(contentItems);
        console.log(storedConsonants.join(""));
        break;
    }
}
